//
//  HomeViewModelTests.swift
//  artInstituteTests
//
//  Created by Ahmed Ezz on 12/02/2022.
//

import XCTest

@testable import artInstitute

class HomeViewModelTests: XCTestCase {
    
    private var artApiManager: MockArtApiManager!
    private var homeViewModel: HomeViewModel!
    private var currentPage: Int!
    private var limit: Int!
    
    override func setUpWithError() throws {
        currentPage = 1
        limit = 12
        artApiManager = MockArtApiManager()
        homeViewModel = HomeViewModel(apiManager: artApiManager)
    }
    
    //MARK: - FetchArtData_onAPIFailure_onFailureClosureIsCalled
    func test_fetchArtData_onAPIFailure_onFailureClosureIsCalled() {
        // Given
        var isFailureClosureIsCalled = false
        artApiManager.responseResult = nil
        // When
        homeViewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: { _ in
        }, onFailure: { _ in
            isFailureClosureIsCalled = true
        })
        // Then
        XCTAssertTrue(isFailureClosureIsCalled)
    }
    
    //MARK: - FetchArtData_onAPIFailure_onSuccessClosureIsNotCalled
    func test_fetchArtData_onAPIFailure_onSuccessClosureIsNotCalled() {
        // Given
        var isSuccessClosureIsCalled = false
        artApiManager.responseResult = nil
        // When
        homeViewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: { _ in
            isSuccessClosureIsCalled = true
        }, onFailure: { _ in
            
        })
        // Then
        XCTAssertFalse(isSuccessClosureIsCalled)
    }
    
    //MARK: - FetchArtData_onAPISuccess_onSuccessClosureIsCalled
    func test_fetchArtData_onAPISuccess_onSuccessClosureIsCalled() {
        // Given
        var isSuccessClosureIsCalled = false
        artApiManager.responseResult = getResponseResult()
        // When
        homeViewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: { _ in
            isSuccessClosureIsCalled = true
        }, onFailure: { _ in
            
        })
        // Then
        XCTAssertTrue(isSuccessClosureIsCalled)
    }
    
    //MARK: - FetchArtData_onAPISuccess_onFailureClosureIsCalled
    func test_fetchArtData_onAPISuccess_onFailureClosureIsCalled() {
        // Given
        var isFailureClosureIsCalled = false
        artApiManager.responseResult = getResponseResult()
        // When
        homeViewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: { _ in
        }, onFailure: { _ in
            isFailureClosureIsCalled = true
        })
        // Then
        XCTAssertFalse(isFailureClosureIsCalled)
    }
    
    //MARK: - FetchMoreArtData_onLoadMore_onCurrentPageLowerTotalPages
    func test_fetchMoreArtData_onLoadMore_onCurrentPageLowerTotalPages() {
        // Given
        var canLoadMore = false
        artApiManager.responseResult = getResponseResult()
        // When
        homeViewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: { _ in
        }, onFailure: { _ in
            
        })
        
        canLoadMore = homeViewModel.canLoadMore(currentPage: currentPage)
        // Then
        XCTAssertTrue(canLoadMore)
    }
    
    //MARK: - FetchMoreArtData_onLoadMore_onCurrentPageEqualTotalPages
    func test_fetchMoreArtData_onLoadMore_onCurrentPageEqualTotalPages() {
        // Given
        var canLoadMore = false
        artApiManager.responseResult = getResponseResult()
        currentPage = 11
        // When
        homeViewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: { _ in
        }, onFailure: { _ in
            
        })
        
        canLoadMore = homeViewModel.canLoadMore(currentPage: currentPage)
        // Then
        XCTAssertFalse(canLoadMore)
    }
    
    private func getResponseResult()->RequestResponse<[Art]>? {
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: "MockArtResponse", ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
        return try? JSONDecoder().decode(RequestResponse<[Art]>.self, from: jsonData ?? Data())
    }
}
