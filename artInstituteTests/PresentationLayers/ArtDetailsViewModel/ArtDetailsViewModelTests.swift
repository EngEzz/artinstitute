//
//  ArtDetailsViewModelTests.swift
//  artInstituteTests
//
//  Created by Ahmed Ezz on 12/02/2022.
//

import XCTest

@testable import artInstitute

class ArtDetailsViewModelTests: XCTestCase {

    private var artistApiManager: MockArtistApiManager!
    private var artDetailsViewModel: ArtDetailsViewModel!
    private var artCellModel: ArtCellModel!
    private var art: Art!
    private var artist: Artist!
    
    override func setUpWithError() throws {
        artist = Artist(id: nil, title: nil, birth_date: nil, death_date: nil)
        art = Art(title: nil, artist_id: nil, artist_title: nil, imageId: nil)
        artCellModel = ArtCellModel(art: art, imageUrl: nil)
        artistApiManager = MockArtistApiManager()
        artDetailsViewModel = ArtDetailsViewModel(art: artCellModel, apiManager: artistApiManager)
    }
    
    //MARK: - FetchArtData_onAPIFailure_onFailureClosureIsCalled
    func test_fetchArtData_onAPIFailure_onFailureClosureIsCalled() {
        // Given
        var isFailureClosureIsCalled = false
        artistApiManager.responseResult = nil
        // When
        artDetailsViewModel.fetchArtistDetails(onSuccess: {_ in
            
        }, onFailure: { _ in
            isFailureClosureIsCalled = true
        })
        // Then
        XCTAssertTrue(isFailureClosureIsCalled)
    }
    
    //MARK: - FetchArtData_onAPIFailure_onSuccessClosureIsNotCalled
    func test_fetchArtData_onAPIFailure_onSuccessClosureIsNotCalled() {
        // Given
        var isSuccessClosureIsCalled = false
        artistApiManager.responseResult = nil
        // When
        artDetailsViewModel.fetchArtistDetails(onSuccess: {_ in
            isSuccessClosureIsCalled = true
        }, onFailure: { _ in
            
        })
        // Then
        XCTAssertFalse(isSuccessClosureIsCalled)
    }
    
    //MARK: - FetchArtData_onAPISuccess_onSuccessClosureIsCalled
    func test_fetchArtData_onAPISuccess_onSuccessClosureIsCalled() {
        // Given
        var isSuccessClosureIsCalled = false
        artistApiManager.responseResult = artist
        // When
        artDetailsViewModel.fetchArtistDetails(onSuccess: {_ in
            isSuccessClosureIsCalled = true
        }, onFailure: { _ in
            
        })
        // Then
        XCTAssertTrue(isSuccessClosureIsCalled)
    }
    
    //MARK: - FetchArtData_onAPISuccess_onFailureClosureIsCalled
    func test_fetchArtData_onAPISuccess_onFailureClosureIsCalled() {
        // Given
        var isFailureClosureIsCalled = false
        artistApiManager.responseResult = artist
        // When
        artDetailsViewModel.fetchArtistDetails(onSuccess: {_ in
            
        }, onFailure: { _ in
            isFailureClosureIsCalled = true
        })
        // Then
        XCTAssertFalse(isFailureClosureIsCalled)
    }

}
