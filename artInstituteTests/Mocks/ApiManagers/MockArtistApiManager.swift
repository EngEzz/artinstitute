//
//  MockArtistApiManager.swift
//  artInstituteTests
//
//  Created by Ahmed Ezz on 12/02/2022.
//

import Foundation

@testable import artInstitute

class MockArtistApiManager: ArtistApiManagerProtocol {
    
    var responseResult: Artist?

    func fetchArtistDetails(id: Int?, onSuccess: @escaping ((Artist?) -> ()), onFailure: @escaping ((Error) -> ())) {
        if let responseResult = responseResult {
            onSuccess(responseResult)
        } else {
            let error = NSError()
            onFailure(error)
        }
    }
}
