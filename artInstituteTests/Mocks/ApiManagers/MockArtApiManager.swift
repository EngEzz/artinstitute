//
//  ArtApiManagerMock.swift
//  artInstituteTests
//
//  Created by Ahmed Ezz on 12/02/2022.
//
import Foundation

@testable import artInstitute

class MockArtApiManager: ArtApiManagerProtocol {
    
    var responseResult: RequestResponse<[Art]>?
    
    func fetchArtData(pageNumber: Int, limit: Int, onSuccess: @escaping ((RequestResponse<[Art]>) -> Void), onFailure: @escaping ((Error) -> ())) {
        if let responseResult = responseResult {
            onSuccess(responseResult)
        } else {
            let error = NSError()
            onFailure(error)
        }
    }
}
