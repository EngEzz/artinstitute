//
//  UIViewEx.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//
import UIKit

extension UIView {
    
    func addDimmedView() {
        let blurView = UIView()
        blurView.backgroundColor = UIColor.black
        blurView.alpha = 0.5
        blurView.tag = 100
        addSubview(blurView)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        blurView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        blurView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        blurView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    func removeDimmedView() {
        self.viewWithTag(100)?.removeFromSuperview()
    }
}
