//
//  UIImageViewEx.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import SDWebImageWebPCoder

extension UIImageView {
    
    func loadImageFromUrl (imgUrl : String?, defaultImage: ImageAsset? = .placeHolder){
        let defaultImage = UIImage(named: defaultImage?.rawValue ?? "")
        if imgUrl == "" || imgUrl == nil {
            self.image = defaultImage
            return
        }
        
        let webPCoder = SDImageWebPCoder.shared
        SDImageCodersManager.shared.addCoder(webPCoder)
        let imgUrl = imgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.replacingOccurrences(of: "\\", with: "")
        guard let url = URL(string: imgUrl ?? "" ) else {
            self.image = defaultImage
            return
        }
        
        DispatchQueue.main.async {[weak self] () in
            self?.sd_setImage(with: url, placeholderImage: defaultImage, options: .highPriority, progress: nil, completed: nil)
        }
    }
    
}

extension UIImage {
    
    convenience init?(assetIdentifier: ImageAsset) {
        self.init(named:assetIdentifier.rawValue,in:Bundle.main, compatibleWith: nil)
    }
}

enum ImageAsset: String {
    case placeHolder = "placeHolder-image"
}
