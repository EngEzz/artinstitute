//
//  BaseViewController.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//
import UIKit

//MARK: - BaseViewControllerProtocol
protocol BaseViewControllerProtocol: AnyObject {
    func showLoading(view: UIView?)
    func hideLoading(view: UIView?)
    func showGeneralAlert(viewController: UIViewController?, message: String?, title: String?, actions: [String:UIAlertAction.Style], onComplete: ((Int)->Void)?)
}

//MARK: - BaseViewControllerProtocol
extension BaseViewControllerProtocol {
    
    //MARK: - ShowLoading
    func showLoading(view: UIView?) {
        let loadingIndicator = UIActivityIndicatorView(frame: view?.bounds ?? CGRect())
        loadingIndicator.tag = 009
        loadingIndicator.autoresizingMask = [
            .flexibleLeftMargin, .flexibleRightMargin,
            .flexibleTopMargin, .flexibleBottomMargin
        ]
        if #available(iOS 13.0, *) {
            loadingIndicator.style = .large
        } else {
            loadingIndicator.style = .whiteLarge
        }
        loadingIndicator.center = CGPoint(x: view?.bounds.midX ?? 0,y: view?.bounds.midY ?? 0)
        loadingIndicator.startAnimating()
        view?.addDimmedView()
        view?.addSubview(loadingIndicator)
        view?.bringSubviewToFront(loadingIndicator)
        view?.isUserInteractionEnabled = false
    }
    
    //MARK: - HideLoading
    func hideLoading(view: UIView?) {
        view?.isUserInteractionEnabled = true
        view?.removeDimmedView()
        view?.viewWithTag(009)?.removeFromSuperview()
    }
    
    //MARK: - ShowGeneralAlert
    func showGeneralAlert(viewController: UIViewController?, message: String? = nil, title: String? = nil, actions: [String:UIAlertAction.Style], onComplete: ((Int)->Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (actionNumber,action) in actions.enumerated() {
            alertController.addAction(UIAlertAction(title: action.key , style: action.value , handler: {[weak self] _ in
                guard let _ = self else {return}
                if action.value != .cancel {
                    onComplete?(actionNumber)
                }
            }))
        }
        alertController.popoverPresentationController?.sourceView = viewController?.view
        viewController?.present(alertController, animated: true, completion: nil)
    }
}
