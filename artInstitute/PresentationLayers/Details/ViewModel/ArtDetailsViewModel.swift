//
//  ArtDetailsViewModel.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 12/02/2022.
//

class ArtDetailsViewModel {

    //MARK: - Constants
    private let apiManager: ArtistApiManagerProtocol
    let art: ArtCellModel?
    
    //MARK: - Initialize
    init(art: ArtCellModel?, apiManager: ArtistApiManagerProtocol) {
        self.art = art
        self.apiManager = apiManager
    }
    
    func fetchArtistDetails(onSuccess: @escaping ((Artist?)->Void), onFailure: @escaping ((Error)->())) {
        apiManager.fetchArtistDetails(id: art?.getArtistId(), onSuccess: onSuccess, onFailure: onFailure)
    }
}
