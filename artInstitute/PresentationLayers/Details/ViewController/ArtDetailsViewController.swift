//
//  ArtDetailsViewController.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 12/02/2022.
//

import CoreMotion

import UIKit

class ArtDetailsViewController: UIViewController, BaseViewControllerProtocol {

    //MARK: - Outlets
    @IBOutlet private weak var showMoreButton: UIButton!
    
    @IBOutlet private weak var artistDeathDateLabel: UILabel!
    @IBOutlet private weak var artistDeathDateTitleLabel: UILabel!
    @IBOutlet private weak var artistBirthDateLabel: UILabel!
    @IBOutlet private weak var artistBirthDateTitleLabel: UILabel!
    @IBOutlet private weak var artistNameLabel: UILabel!
    @IBOutlet private weak var artNameLabel: UILabel!
    
    @IBOutlet private weak var mainStackView: UIStackView!
    @IBOutlet private weak var artistDetailsStackView: UIStackView!
    
    @IBOutlet private weak var artImageView: UIImageView! {
        didSet {
            self.artImageView.contentMode = .scaleAspectFill
        }
    }
    
    //MARK: - Constants
    private let viewModel: ArtDetailsViewModel
    private let errorAlertTitle = "Error"
    private let alertActionTitle = "Ok"
    private let motion = CMMotionManager()

    //MARK: - Variables
    private var fontTextSize: CGFloat = FontText.regular.rawValue
    private var loadingIndicator: UIActivityIndicatorView!

    //MARK: - Initialize
    init(viewModel: ArtDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "\(ArtDetailsViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.configureLabelsFont(fontSize: fontTextSize)
        self.configureShowMoreButton()
        self.bindData()
        self.listenToGyroscopeUpdates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func configureArtistLabels() {
        artistBirthDateTitleLabel.text = "Artist birthDate"
        artistDeathDateTitleLabel.text = "Artist deathDate"
        artistDeathDateTitleLabel.textColor = .lightGray
        artistBirthDateTitleLabel.textColor = .lightGray
    }
    
    //MARK: - ConfigureUI
    private func configureLabelsFont(fontSize: CGFloat) {
        let allSubViews = subviewsRecursive(view: mainStackView)
        let _ = allSubViews.filter{$0.isKind(of: UILabel.self)}.forEach{view in
            let label = view as? UILabel
            label?.font = UIFont.systemFont(ofSize: fontSize)
        }
    }

    private func subviewsRecursive(view: UIView) -> [UIView] {
        return view.subviews + view.subviews.flatMap { subviewsRecursive(view: $0) }
    }
    
    private func configureShowMoreButton() {
        showMoreButton.setTitleColor(.white, for: .normal)
        showMoreButton.backgroundColor = .black
        showMoreButton.layer.cornerRadius = 12
        showMoreButton.clipsToBounds = true
        addLoadingIndicator()
    }
    
    private func addLoadingIndicator() {
        loadingIndicator = UIActivityIndicatorView()
        showMoreButton.addSubview(loadingIndicator)
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loadingIndicator.leadingAnchor.constraint(equalTo: showMoreButton.leadingAnchor,constant: 10),
            loadingIndicator.centerYAnchor.constraint(equalTo: showMoreButton.centerYAnchor),
            loadingIndicator.heightAnchor.constraint(equalToConstant: 20),
            loadingIndicator.widthAnchor.constraint(equalToConstant: 20)
        ])
        loadingIndicator.isHidden = true
        loadingIndicator.color = .white
    }
    
    //MARK: - DataBinding
    private func bindData() {
        self.showMoreButton.isHidden = viewModel.art?.getArtistId() == nil
        self.artImageView.loadImageFromUrl(imgUrl: viewModel.art?.getImageUrl())
        self.artNameLabel.text = viewModel.art?.getArtName()
        self.artistNameLabel.text = viewModel.art?.getArtistName()
        self.artistDetailsStackView.isHidden = true
    }
    
    private func loadArtistData(artist: Artist?) {
        self.configureArtistLabels()
        self.artistDetailsStackView.isHidden = false
        self.artistBirthDateLabel.text = artist?.birth_date?.description
        self.artistDeathDateLabel.text = artist?.death_date?.description
    }
    
    //MARK: - FetchArtistData
    private func fetchArtistData() {
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
        viewModel.fetchArtistDetails(onSuccess: {[weak self] artist in
            DispatchQueue.main.async {[weak self] in
                self?.loadingIndicator.isHidden = true
                self?.loadingIndicator.stopAnimating()
                self?.loadArtistData(artist: artist)
            }
        }, onFailure: {[weak self] error in
            DispatchQueue.main.async {[weak self] in
                self?.loadingIndicator.isHidden = true
                self?.loadingIndicator.stopAnimating()
                self?.showGeneralAlert(viewController: self, message: error.localizedDescription, title: self?.errorAlertTitle, actions: [self?.alertActionTitle ?? "" :.default], onComplete: nil)
            }
        })
    }
    
    //MARK: - GyroscopeUpdates
    private func listenToGyroscopeUpdates() {
        if motion.isDeviceMotionAvailable {
            motion.startDeviceMotionUpdates(to: .main, withHandler: {[weak self] data,error in
                let angel = ((data?.attitude.yaw ?? 0) * 180) / .pi
                if angel > 30 {
                    self?.configureLabelsFont(fontSize: FontText.leftDirection.rawValue)
                    self?.view.backgroundColor = .green
                } else if angel < -30 {
                    self?.configureLabelsFont(fontSize: FontText.rightDirection.rawValue)
                    self?.view.backgroundColor = .red
                } else {
                    self?.configureLabelsFont(fontSize: FontText.regular.rawValue)
                    self?.view.backgroundColor = .white
                }
            })
        }
    }
    
    //MARK: - IBAction
    @IBAction func showMore(_ sender: UIButton) {
        self.fetchArtistData()
    }
    
}

private enum FontText: CGFloat {
    case regular = 16
    case rightDirection = 30
    case leftDirection = 12
}
