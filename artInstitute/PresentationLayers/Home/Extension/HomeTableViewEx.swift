//
//  HomeTableViewEx.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import UIKit

extension HomeViewController: UITableViewDataSource, UITableViewDelegate, UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = dataSource[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: artCellIdentifier, for: indexPath) as? ArtTableViewCell {
            cell.cellModel = cellModel
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if self.viewModel.canLoadMore(currentPage: currentPage) && indexPaths.last?.row == self.dataSource.count - 1 {
            self.currentPage += 1
            self.retrieveData(onSuccess: {[weak self] artWorks in
                self?.reloadData(artWorks: artWorks)
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellModel = dataSource[indexPath.row]
        let apiManager = ArtistApiManager()
        let viewModel = ArtDetailsViewModel(art: cellModel, apiManager: apiManager)
        let viewController = ArtDetailsViewController(viewModel: viewModel)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
