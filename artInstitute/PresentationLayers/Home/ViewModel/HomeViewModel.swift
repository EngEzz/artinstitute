//
//  HomeViewModel.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

class HomeViewModel {
    
    //MARK: - Constants
    private let apiManager: ArtApiManagerProtocol
    
    //MARK: - Variables
    private var pagination: Pagination?
    var imageUrl: String?
    
    //MARK: - Initialize
    init(apiManager: ArtApiManagerProtocol) {
        self.apiManager = apiManager
    }
    
    //MARK: - ArtData
    func fetchArtData(page: Int, limit: Int, onSuccess: @escaping (([Art])->Void), onFailure: @escaping ((Error)->Void)) {
        apiManager.fetchArtData(pageNumber: page, limit: limit, onSuccess: {[weak self] result in
            self?.pagination = result.pagination
            self?.imageUrl = result.config?.imageUrl
            onSuccess(result.data ?? [])
        }, onFailure: onFailure)
    }
    
    //MARK: - canLoadMore
    func canLoadMore(currentPage: Int)-> Bool {
        return currentPage < pagination?.total_pages ?? 0
    }
}
