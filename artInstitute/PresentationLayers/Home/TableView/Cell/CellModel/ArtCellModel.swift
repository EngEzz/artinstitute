//
//  ArtCellModel.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//
import Foundation

class ArtCellModel {
    
    //MARK: - Constants
    private let art: Art?
    private let imageUrl: String?
    private let imagePath = "/full/843,/0/default.jpg"
    
    //MARK: - Initilaize
    init(art: Art?, imageUrl: String?) {
        self.art = art
        self.imageUrl = imageUrl
    }
    
    //MARK: - EntityData
    func getArtName()-> String? {
        return art?.title
    }
    
    func getArtistName()-> String? {
        return art?.artist_title
    }
    
    func getArtistId()-> Int? {
        return art?.artist_id
    }
    
    func getImageUrl()-> String? {
        return "\(imageUrl ?? "")/\(art?.imageId ?? "")\(imagePath)"
    }
}
