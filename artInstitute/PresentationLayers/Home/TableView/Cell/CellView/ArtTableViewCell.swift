//
//  ArtTableViewCell.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import UIKit

class ArtTableViewCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet private weak var artistNameLabel: UILabel!
    @IBOutlet private weak var artNameLabel: UILabel!
    
    @IBOutlet private weak var artImageView: UIImageView! {
        didSet {
            self.artImageView.contentMode = .scaleAspectFit
            self.artImageView.clipsToBounds = true
            self.artImageView.layer.cornerRadius = self.artImageView.bounds.width / 2
            self.artImageView.layer.borderWidth = 0.5
            self.artImageView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    var cellModel: ArtCellModel? {
        didSet {
            self.bindData()
        }
    }
    
    //MARK: - Nib
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.configureArtNameLabel()
        self.configureArtistNameLabel()
    }
    
    //MARK: - ConfigureUI
    private func configureArtNameLabel() {
        self.artNameLabel.textColor = .black
        self.artNameLabel.font = UIFont.systemFont(ofSize: 15)
    }
    
    private func configureArtistNameLabel() {
        self.artistNameLabel.textColor = .lightGray
        self.artistNameLabel.font = UIFont.systemFont(ofSize: 12)
    }
    
    //MARK: - bindData
    private func bindData() {
        if let imageUrl = cellModel?.getImageUrl() {
            self.artImageView.loadImageFromUrl(imgUrl: imageUrl)
        }
        self.artistNameLabel.text = cellModel?.getArtistName()
        self.artNameLabel.text = cellModel?.getArtName()
    }
    
}
