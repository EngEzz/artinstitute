//
//  HomeViewController.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import UIKit

class HomeViewController: UIViewController, BaseViewControllerProtocol {

    //MARK: - Outlet
    @IBOutlet private weak var tableView: UITableView!
    
    //MARK: - Constants
    private(set) var viewModel: HomeViewModel
    private let limit = 12
    private let errorAlertTitle = "Error"
    private let alertActionTitle = "Ok"
    let artCellIdentifier = "\(ArtTableViewCell.self)"
    
    //MARK: - Variables
    private(set) var dataSource: [ArtCellModel] = []
    var currentPage = 1
    
    //MARK: - Initialize
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "\(HomeViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.fetchArtData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    //MARK: - ConfigureUI
    private func configureTableView() {
        tableView.register(UINib(nibName: artCellIdentifier, bundle: nil), forCellReuseIdentifier: artCellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.prefetchDataSource = self
    }
    
    //MARK: - FetchData
    private func fetchArtData() {
        self.showLoading(view:view)
        self.retrieveData(onSuccess: {[weak self] result in
            DispatchQueue.main.async {[weak self] in
                self?.hideLoading(view: self?.view)
                self?.reloadData(artWorks: result)
            }
        },onFailure: {[weak self] in
            DispatchQueue.main.async {[weak self] in
                self?.hideLoading(view: self?.view)
            }
        })
    }
    
    func retrieveData(onSuccess:@escaping(([Art])-> Void), onFailure:(()->Void)? = nil ) {
        self.viewModel.fetchArtData(page: currentPage, limit: limit, onSuccess: {[weak self] artWorks in
            guard let _ = self else {return}
            onSuccess(artWorks)
        }, onFailure: {[weak self] error in
            DispatchQueue.main.async { [weak self] in
                self?.showGeneralAlert(viewController: self, message: error.localizedDescription, title: self?.errorAlertTitle, actions: [self?.alertActionTitle ?? "" :.default], onComplete: nil)
            }
        })
    }
    
    //MARK: - ReloadData
    func reloadData(artWorks: [Art]) {
        self.dataSource.append(contentsOf: artWorks.map{ArtCellModel(art: $0, imageUrl: self.viewModel.imageUrl)})
        DispatchQueue.main.async {[weak self] in
            UIView.performWithoutAnimation {[weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
