//
//  ArtApiManager.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import Foundation

class ArtApiManager: NetworkManager, ArtApiManagerProtocol {
    
    //MARK: - Constants
    private let pageKey = "page"
    private let limitKey = "limit"
    
    private enum EndPoint: String {
        case artworks
    }
    
    //MARK: - FetchArtData
    func fetchArtData(pageNumber: Int, limit: Int, onSuccess: @escaping ((RequestResponse<[Art]>)->Void), onFailure: @escaping ((Error)->())) {
        let queryParams = [pageKey: pageNumber, limitKey: limit]
        let router = Router(httpMethod: .get, urlPath: EndPoint.artworks.rawValue,queryParams: queryParams)
        super.performRequest(router: router, dataType: RequestResponse<[Art]>.self, decoder: JSONDecoder(), onComplete: { [weak self] result in
            guard let _ = self else {return}
            switch result {
            case .success(let response):
                onSuccess(response)
            case .failure(let error):
                onFailure(error)
            }
        })
    }
}

//MARK: - ArtApiManagerProtocol
protocol ArtApiManagerProtocol: AnyObject {
    func fetchArtData(pageNumber: Int, limit: Int, onSuccess: @escaping ((RequestResponse<[Art]>)->Void), onFailure: @escaping ((Error)->()))
}
