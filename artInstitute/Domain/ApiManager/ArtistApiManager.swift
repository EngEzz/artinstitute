//
//  ArtistApiManager.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import UIKit

class ArtistApiManager: NetworkManager, ArtistApiManagerProtocol {
    
    //MARK: - Cosntants
    private enum EndPoint: String {
        case details = "artists"
    }
    
    //MARK: - FetchArtistDetails
    func fetchArtistDetails(id: Int?, onSuccess: @escaping ((Artist?) -> ()), onFailure: @escaping ((Error) -> ())) {
        let router = Router(httpMethod: .get, urlPath: "\(EndPoint.details.rawValue)/\(id ?? 0)")
        super.performRequest(router: router, dataType: RequestResponse<Artist>.self, decoder: JSONDecoder(), onComplete: { [weak self] result in
            guard let _ = self else {return}
            switch result {
            case .success(let response):
                onSuccess(response.data)
            case .failure(let error):
                onFailure(error)
            }
        })
    }
}

//MARK: - ArtistApiManagerProtocol
protocol ArtistApiManagerProtocol: AnyObject {
    func fetchArtistDetails(id: Int?, onSuccess: @escaping ((Artist?)->()), onFailure: @escaping( (Error)->()))
}
