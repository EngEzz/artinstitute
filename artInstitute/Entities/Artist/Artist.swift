//
//  Artist.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

struct Artist: Decodable {
    
    let id : Int?
    let title : String?
    let birth_date : Int?
    let death_date : Int?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case title = "title"
        case birth_date = "birth_date"
        case death_date = "death_date"
    }
}
