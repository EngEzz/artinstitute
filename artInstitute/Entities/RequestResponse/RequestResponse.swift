//
//  RequestResponse.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

struct RequestResponse<T: Decodable>: Decodable {
    
    let pagination: Pagination?
    let data: T?
    let config: Config?
    
    enum CodingKeys: String, CodingKey {
        case pagination = "pagination"
        case data = "data"
        case config = "config"
    }
}
