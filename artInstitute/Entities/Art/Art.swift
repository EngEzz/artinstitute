//
//  Art.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

struct Art: Decodable {
    
    let title : String?
    let artist_id : Int?
    let artist_title : String?
    let imageId : String?
    
    enum CodingKeys: String, CodingKey {

        case title = "title"
        case artist_id = "artist_id"
        case artist_title = "artist_title"
        case imageId = "image_id"
    }

}
