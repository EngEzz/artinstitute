//
//  Pagination.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

struct Pagination : Decodable {
    
	let total : Int?
	let limit : Int?
	let offset : Int?
	let total_pages : Int?
	let current_page : Int?
	let next_url : String?

	enum CodingKeys: String, CodingKey {

		case total = "total"
		case limit = "limit"
		case offset = "offset"
		case total_pages = "total_pages"
		case current_page = "current_page"
		case next_url = "next_url"
	}
}
