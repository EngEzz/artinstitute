//
//  Config.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

struct Config: Decodable {
    
    let imageUrl : String?
    
    enum CodingKeys: String, CodingKey {
        case imageUrl = "iiif_url"
    }
}
