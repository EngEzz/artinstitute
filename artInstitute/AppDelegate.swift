//
//  AppDelegate.swift
//  artInstitute
//
//  Created by Ahmed Ezz on 11/02/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.loadMainController()
        return true
    }
    
    private func loadMainController() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let artApiManager = ArtApiManager()
        let homeViewModel = HomeViewModel(apiManager: artApiManager)
        let viewController = HomeViewController(viewModel: homeViewModel)
        self.window?.rootViewController = UINavigationController(rootViewController: viewController)
        self.window?.makeKeyAndVisible()
    }

}

