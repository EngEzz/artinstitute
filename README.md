# Artinstitute Task #

This README would describe how i completed this task.

### How do you get the app up and running ? ###

-  Clone the app `git clone https://EngEzz@bitbucket.org/EngEzz/artinstitute.git`
- Go to Xcode and import the project.
- Open the project directory and run this command  `pod install`.
- Run the app.
 

### What 3rd party libraries did I use and why ? ###
* SDWebImageWebPCoder --> I used for it handling loading image from url.


### What architecture did I use and why ? ###
-  MVVM stands for (Model-View-ViewModel) this pattern is highly scalable and has a brilliant separation of concerns. One feature, one module.
- For each module has three different classes with distinct roles. No class go beyond its sole purpose. These classes are following. 

###  Describe the process that you would follow in your current place of work to bring this from an initial idea to a live feature in production. What are the positives and negatives of this approach ?  ###
- get requirements and user stories for that new feature.
- Start to design architecture we will use and design our screens.
- Start to implement this feature.
- After we finish the  implementation , we start running our test cases.
- After we finish the testing and confirm everything is working , we deploy this feature.
- Positivies --> We can deploy this feature faster , detect and fix issues faster and get feedback quickly.
- Negatives --> We can`t estimate time we will need and don't know the full scope of requirements.


### Created by ###

* [ Ahmed Ezz ](https://www.linkedin.com/in/ahmed-ezz-mobile-developer/)
